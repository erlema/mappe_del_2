module HospitalApplication {
    requires javafx.base;
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.fxml;
    opens HospitalApplication.Controllers to javafx.fxml;
    opens HospitalApplication.Data to javafx.base;
    exports HospitalApplication;
}