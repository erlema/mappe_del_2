package HospitalApplication;

import HospitalApplication.Controllers.MainView;
import HospitalApplication.Data.Patient;
import HospitalApplication.Data.PatientRegister;
import HospitalApplication.Files.FileHandler;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.ArrayList;

/**
 * Client class extends Application
 *
 * Contains the information of the patient register.
 * A binding between controllers and the data (Could be changed)
 *
 * @author Erlend Matre
 */
public class Client extends Application {

    private static PatientRegister patientRegister = new PatientRegister();
    private static Stage stage;

    /**
     * Starts the application
     * @param stage main page
     */
    @Override
    public void start(Stage stage){
        //addPatient(new Patient("JA","OK","12", "MAN"));
        MainView mainView = (MainView) FileHandler.loadView("MainView");
        this.stage = stage;
        this.stage.setTitle("Patient Register");
        this.stage.setScene(new Scene((Parent) mainView.getParent()));
        this.stage.setResizable(false);
        this.stage.show();
    }

    /**
     * Launches the application
     * @param args args
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Static method to add patient to patient register
     *
     * @param firstName First Name
     * @param lastName Last Name
     * @param socialSecurityNumber SSN
     * @param nameOfGenPractitioner Gen. Practitioner
     * @return True or False
     */
    public static boolean addPatient(String firstName, String lastName, String socialSecurityNumber, String nameOfGenPractitioner){
        if(!checkPatientInfo(firstName,lastName,socialSecurityNumber,nameOfGenPractitioner)){ return false;}
        return patientRegister.addPatient(firstName,lastName,socialSecurityNumber,nameOfGenPractitioner);
    }

    /**
     * Static method to remove a patient if he is in register
     *
     * @param removePatient Patient to be removed
     * @return True or false
     */
    public static boolean removePatient(Patient removePatient){ return patientRegister.removePatient(removePatient); }

    /**
     * Static method to edit a patient
     *
     * @param editPatient Patient to edit
     * @param newFirstName New first name
     * @param newLastName New last name
     * @param newSocialSecurityNumber new SSN (maybe should be final)
     * @param newGeneralPractitionersName new Gen. Practitioner name
     * @return True or False
     */
    public static boolean editPatient(Patient editPatient, String newFirstName, String newLastName, String newSocialSecurityNumber, String newGeneralPractitionersName){
        if(!checkPatientInfo(newFirstName,newLastName,newSocialSecurityNumber,newGeneralPractitionersName)) { return false;}
        if(patientRegister.getPatientRegister().contains(editPatient)){
            editPatient.setFirstName(newFirstName);
            editPatient.setLastName(newLastName);
            editPatient.setSocialSecurityNumber(newSocialSecurityNumber);
            editPatient.setGeneralPractitionersName(newGeneralPractitionersName);
            return true;
        }
        return false;
    }

    /**
     * @return Patient register
     */
    public static ArrayList<Patient> getPatients(){
        return patientRegister.getPatientRegister();
    }

    /**
     * Sets the diagnosis of patient
     * @param patient Patient
     * @param diagnosis Diagnosis
     * @return True or false
     */
    public static boolean setDiagnosis(Patient patient, String diagnosis){
        if(diagnosis.trim().isEmpty()){return false;}
        if(patientRegister.getPatientRegister().contains(patient)){
            patient.setDiagnosis(diagnosis);
            return true;
        }
        return false;
    }

    /**
     * Method that checks patient info
     *
     * Does not check for other symbols in Strings, as problems with imported Patient.csv occurs
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     * @param nameOfGenPractitioner
     * @return True or false
     */

    private static boolean checkPatientInfo(String firstName, String lastName, String socialSecurityNumber, String nameOfGenPractitioner){
        if(firstName.trim().isEmpty()||firstName.length()>20){
            return false;
        }
        if(lastName.trim().isEmpty()||lastName.length()>20){
            return false;
        }
        if(!(socialSecurityNumber.length() ==11)||!(socialSecurityNumber.matches("\\d+"))){
            return false;
        }
        if(nameOfGenPractitioner.trim().isEmpty()||nameOfGenPractitioner.trim().length()>35){
            return false;
        }
        return true;
    }

    /**
     * Closes application
     */
    public static void exitApp(){
        stage.close();
    }
}

