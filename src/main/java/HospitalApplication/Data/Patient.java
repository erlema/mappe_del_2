package HospitalApplication.Data;

import java.util.Objects;

/**
 * Patient class
 *
 * Class representing a patient
 *
 * @author Erlend Matre
 */
public class Patient {

    private String firstName;
    private String lastName;
    private String socialSecurityNumber;
    private String diagnosis = null;
    private String generalPractitionersName;

    /**
     * Constructor of patient
     *
     * Does not check for any wrong inputs
     *
     * @param firstName First name of patient
     * @param lastName Last name of patient
     * @param socialSecurityNumber Social security number to the patient
     * @param nameOfGenPractitioner Gen. Practitioner of Patient
     */
    public Patient(String firstName, String lastName, String socialSecurityNumber, String nameOfGenPractitioner){
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
        this.generalPractitionersName = nameOfGenPractitioner;
    }

    /**
     * First name getter
     * @return First name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * First name setter
     * @param firstName
     */

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Last name getter
     * @return last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Last name setter
     * @param lastName
     */

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * SSN setter
     * @param socialSecurityNumber
     */

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber; }

    /**
     * SSN getter
     * @return SSN
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * Diagnosis getter
     * @return diagnosis
     */
    public String getDiagnosis() {
        return diagnosis;
    }

    /**
     * diagnosis setter
     * @param diagnosis
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    /**
     * Gen. Practitioner's name getter
     * @return Name
     */
    public String getGeneralPractitionersName() {
        return generalPractitionersName;
    }

    /**
     * Gen. Practitioner's name setter
     * @param generalPractitionersName
     */

    public void setGeneralPractitionersName(String generalPractitionersName) {
        this.generalPractitionersName = generalPractitionersName;
    }

    /**
     * Checks for the same SSN
     * @param o
     * @return true/false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return socialSecurityNumber.equals(patient.socialSecurityNumber);
    }

    /**
     * Hashcode of Patient
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(socialSecurityNumber);
    }
}
