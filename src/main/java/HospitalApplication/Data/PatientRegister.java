package HospitalApplication.Data;

import java.util.ArrayList;

/**
 * Patient register class
 *
 * Class for all the patient in the application
 *
 * @author Erlend Matre
 */
public class PatientRegister {

    public ArrayList<Patient> patientRegister;

    /**
     * Constructor for the class
     *
     * Creates a new register
     */
    public PatientRegister(){
        patientRegister = new ArrayList<>();
    }

    /**
     * Method to add patient in register
     * Only adds if there is not a patient with same SSN
     *
     * @param firstName First name of patient
     * @param lastName Last name of patient
     * @param socialSecurityNumber Social security number to the patient
     * @param nameOfGenPractitioner Gen. Practitioner of Patient
     * @return True or false depending on if patient was added successfully
     */
    public boolean addPatient(String firstName, String lastName, String socialSecurityNumber, String nameOfGenPractitioner){
        Patient newPatient = new Patient(firstName,lastName,socialSecurityNumber, nameOfGenPractitioner);
        if(patientRegister.contains(newPatient)){return false;}
        patientRegister.add(newPatient);
        return true;
    }

    /**
     * Method to remove patient in register
     *
     * @param removedPatient Patient to be removed
     * @return True or false depending on if patient was added successfully
     */
    public boolean removePatient(Patient removedPatient){
        if(!patientRegister.contains(removedPatient)){return false;}

        patientRegister.remove(removedPatient);
        return true;
    }

    /**
     * Method for getting all the patients
     *
     * @return Patient register
     */
    public ArrayList<Patient> getPatientRegister() {
        return patientRegister;
    }

}
