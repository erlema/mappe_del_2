package HospitalApplication.Factory;

import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * Factory class that returns a Node
 * which can be used in the a scene.
 *
 * @author Erlend Matre
 */
public abstract class NodeFactory {

    /**
     * A method to create a Node to be used in JavaFX
     *
     * The Nodes that have been included here are the ones
     * that I personally considered using or is currently using
     *
     * @param nodeName, the name of the wanted Node
     * @return Node which was called, or null if it is not a valid Node
     */

    //TODO Add more Nodes that could be useful and also take more input to make factory more useful, for example height and width
    public Node createNode(String nodeName){
        switch(nodeName.toUpperCase()){
            case "BUTTON":
                return new Button();
            case "TABLEVIEW":
                return new TableView<>();
            case "MENUBAR":
                return new MenuBar();
            case "ANCHORPANE":
                return new AnchorPane();
            case "SCROLLPANE":
                return new ScrollPane();
            case "HBOX":
                return new HBox();
            case "VBOX":
                return new VBox();
            case "LABEL":
                return new Label();
            case "TOOLBAR":
                return new ToolBar();
            case "BORDERPANE":
                return new BorderPane();
            default:
                return null;
        }
    }
}
