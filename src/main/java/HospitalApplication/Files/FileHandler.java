package HospitalApplication.Files;

import HospitalApplication.Client;
import HospitalApplication.Controllers.View;
import HospitalApplication.Data.Patient;
import javafx.fxml.FXMLLoader;


import java.io.*;

/**
 * FileHandler class
 *
 * Class to handle file management
 *
 * @author Erlend Matre
 */
public class FileHandler {
    /**
     * Current save format
     */
    private static String saveFormat = "FirstName;LastName;GeneralPractioner;SocialSecurityNumber;";

    /**
     * Loads an FXML file controller, all of which extends View
     *
     * @param name Name of the fxml file
     * @return The controller class.
     */
    public static View loadView(String name) {
        FXMLLoader fxmlLoader = new FXMLLoader(FileHandler.class.getResource("/fxml/" + name + ".fxml"));
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fxmlLoader.getController();
    }

    /**
     * Method for saving a file, saved in a special format (FirstName;LastName;GeneralPractioner;SocialSecurityNumber;)
     * Does not save the diagnosis for the patient, as it the Patitent.csv file does not contain it.
     *
     * //TODO Save diagnosis
     * Takes in csv file to overwrite.
     *
     * @param file csv file
     * @return True or false depending on if something went wrong
     */
    public static boolean saveCSVFile(File file) {
        try {
            file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.append(saveFormat); //format of the saved information
            for (Patient p: Client.getPatients()) {
                writer.append(p.getFirstName() + ";");
                writer.append(p.getLastName() + ";");
                writer.append(p.getGeneralPractitionersName() + ";");
                writer.append(p.getSocialSecurityNumber() + "\n");

            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Method for loading in a csv file to the patient register
     * Does not save the diagnosis for the patient, as it the Patitent.csv file does not contain it.
     *
     * Only loads correctly in this format: FirstName;LastName;GeneralPractioner;SocialSecurityNumber
     * //TODO Make it adaptable (problem if the first line doesnt have formatting information)
     * //TODO Save diagnosis
     * @param file csv file
     * @return True or false depending on if something went wrong
     */
    public static boolean loadCSVFile(File file){
        boolean fullySuccessful = true;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            String line;
            bufferedReader.readLine(); //Ignores first line
            while ((line = bufferedReader.readLine()) != null) {
                String [] splitString = line.split(";");
                if(Client.addPatient(splitString[0],splitString[1],splitString[3],splitString[2])==false){
                    fullySuccessful =false;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return fullySuccessful;
    }
}
