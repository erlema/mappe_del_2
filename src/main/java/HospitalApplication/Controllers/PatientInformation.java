package HospitalApplication.Controllers;

import HospitalApplication.Client;
import HospitalApplication.Data.Patient;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 * Patient Information class extends View
 *
 * Class for editing or adding a patient
 *
 * @author Erlend Matre
 */
public class PatientInformation extends View{
    @FXML
    private AnchorPane parent;
    @FXML
    private TextField socialSecNumField;
    @FXML
    private TextField firstNameField;
    @FXML
    private TextField lastNameField;
    @FXML
    private TextField genPractitionerField;

    private Patient patient;

    /**
     * Sets the patient if the edit patient button was clicked
     * @param p
     */
    public void setPatient(Patient p) {
        patient = p;
        firstNameField.setText(p.getFirstName());
        lastNameField.setText(p.getLastName());
        socialSecNumField.setText(p.getSocialSecurityNumber());
        genPractitionerField.setText(p.getGeneralPractitionersName());
    }

    /**
     * Adds or edits a patient depending on what button was clicked and closes the page
     */

    @FXML
    private void addPatientButton() {
        if (checkInfo()) {
            if (patient == null) {
                if(!Client.addPatient(firstNameField.getText(), lastNameField.getText(), socialSecNumField.getText(), genPractitionerField.getText())){
                    showWarning("Something went wrong");
                }

            } else {
                if(!Client.editPatient(patient, firstNameField.getText(), lastNameField.getText(), socialSecNumField.getText(),genPractitionerField.getText())){
                    showWarning("Something went wrong");
                }
            }
            MainView.closeExtraStage();
        }
    }

    /**
     * Cancels the operation and closes the page
     */
    @FXML
    private void cancelButton(){
        MainView.closeExtraStage();
    }

    /**
     * Checks if there are any illegal arguments being given in text fields
     *
     * This is being checked here to catch when both creating and editing a patient
     *
     * @return true or false
     */
    private boolean checkInfo(){
        if(firstNameField.getText().trim().isEmpty()){
            showWarning("First name cant be empty");
            return false;
        }
        if(firstNameField.getText().length()>20){
            showWarning("First name can not be over 20 characters");
            return false;
        }
        if(!firstNameField.getText().replaceAll("\\s", "").trim().chars().allMatch(Character::isLetter)){
            showWarning("First name can only contain letters");
            return false;
        }

        if (lastNameField.getText().trim().isEmpty()){
            showWarning("Last name cant be empty");
            return false;
        }
        if (lastNameField.getText().length()>20){
            showWarning("Last name can not be over 20 characters");
            return false;
        }
        if(!lastNameField.getText().replaceAll("\\s", "").trim().chars().allMatch(Character::isLetter)){
            showWarning("Last name can only contain letters");
            return false;
        }
        if (genPractitionerField.getText().trim().isEmpty()){
            showWarning("General Practitioner's name cant be empty");
            return false;
        }
        if (genPractitionerField.getText().length()>35){
            showWarning("General Practitioner's name can not be over 30 characters");
            return false;
        }
        if(!genPractitionerField.getText().replaceAll("\\s","").trim().chars().allMatch(Character::isLetter)){
            showWarning("General Practitioner's name can only contain letters");
            return false;
        }

        if(socialSecNumField.getText().length()<=10 || socialSecNumField.getText().length() >= 12
                || !socialSecNumField.getText().matches("\\d+")){
                    showWarning("Social Security Number must be 11 numbers");
            return false;
        }

        for (Patient p: Client.getPatients()) {
            if(p.getSocialSecurityNumber().equals(socialSecNumField.getText()) && !p.equals(patient)){
                showWarning("There already exist a patient with this Social Security Number");
                return false;
            }
        }
        return true;
    }

    /**
     * Shows a warning alert
     * @param warningText message to be displayed
     */
    private void showWarning(String warningText){
        Alert noPatientClickedWarning = new Alert(Alert.AlertType.WARNING,warningText);
        noPatientClickedWarning.setTitle("Warning!");
        noPatientClickedWarning.show();
    }

    /**
     * Gets the parent of this controller
     * @return AnchorPane (parent)
     */
    @Override
    public Node getParent() {
        return parent;
    }
}
