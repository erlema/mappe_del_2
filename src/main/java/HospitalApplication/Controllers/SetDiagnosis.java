package HospitalApplication.Controllers;

import HospitalApplication.Client;
import HospitalApplication.Data.Patient;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

/**
 * Set Diagnosis class extends View
 *
 * Class for setting or editing diagnosis
 *
 * @author Erlend Matre
 */
public class SetDiagnosis extends View {
    @FXML
    private AnchorPane parent;
    @FXML
    private Text patientName;
    @FXML
    private TextArea diagnosisText;

    private Patient patient;

    /**
     * Sets the patient of the diagnosis change
     * @param patient
     */
    public void setPatient(Patient patient){
        this.patient = patient;
        patientName.setText(this.patient.getFirstName() + " " + this.patient.getLastName());
        diagnosisText.setText(this.patient.getDiagnosis());
    }

    /**
     * Sets diagnosis of patients and closes the page
     */
    @FXML
    public void setDiagnosis(){
        Client.setDiagnosis(patient, diagnosisText.getText());
        MainView.closeExtraStage();
    }

    /**
     * Cancels and closes the page
     */
    @FXML
    private void cancel(){
        MainView.closeExtraStage();
    }

    /**
     * Gets the parent of this controller
     * @return AnchorPane (parent)
     */
    @Override
    public Node getParent() {
        return parent;
    }
}
