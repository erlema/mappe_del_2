package HospitalApplication.Controllers;

import javafx.scene.Node;

/**
 * Abstract class View
 *
 * A class all fxml controllers extends from
 * (Could be replaced by parent, but now it is easier to implement things to all the controllers and add controllers)
 * For easier integration of fxml controllers
 *
 * @author Erlend Matre
 *
 * //Todo Change to a more fitting name
 */
public abstract class View {
    /**
     * @return parent of the controllers
     */
    public abstract Node getParent();
}
