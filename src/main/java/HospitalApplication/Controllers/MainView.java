package HospitalApplication.Controllers;

import HospitalApplication.Client;
import HospitalApplication.Data.Patient;
import HospitalApplication.Files.FileHandler;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

/**
 * Class for the main view of the application extends view
 *
 * @author Erlend Matre
 */
public class MainView extends View {
    @FXML
    private AnchorPane parent;
    @FXML
    private TableView tableView;
    @FXML
    private TableColumn<Patient,String> firstNameColumn;
    @FXML
    private TableColumn<Patient,String> lastNameColumn;
    @FXML
    private TableColumn<Patient,String> SSNColumn;
    @FXML
    private TableColumn<Patient, String> genPractitionerColumn;

    @FXML
    private static Stage extraStage;

    @FXML
    private Label statusLabel;

    private static ObservableList <Patient> observableList;

    private static boolean status = true;
    private FileChooser fileChooser = new FileChooser();

    /**
     * Initializes the page
     */
    @FXML
    private void initialize(){

        this.firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        this.lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        this.SSNColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        this.genPractitionerColumn.setCellValueFactory(new PropertyValueFactory<>("generalPractitionersName"));
        observableList =
                FXCollections.observableArrayList(Client.getPatients());
        tableView.setItems(observableList);

        statusCheck();
    }

    /**
     * Updates the displaying patients
     */
    private static void updateObservableList(){
        observableList.setAll(Client.getPatients());
    }
    @FXML
    /**
     * Creates a popup page to add a patient
     */
    private void addPatientAction(){
        showPatientInformationStage(false);
    }

    /**
     * Removes clicked patient, displays a warning
     */
    @FXML
    private void removePatientAction(){
        if(getClickedPatient() == null){return;}
        Alert deleteAlert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to delete this item?",ButtonType.OK, ButtonType.CANCEL);
        deleteAlert.setTitle("Deletion Confirmation");
        deleteAlert.setHeaderText("Delete Confirmation");
        deleteAlert.showAndWait().ifPresent(buttonType ->
        {
            if(buttonType == ButtonType.OK){
                Client.removePatient(getClickedPatient());
            }
        });
        updateObservableList();
    }

    /**
     * Creates a popup page to edit a clicked patient
     */
    @FXML
    private void editPatientAction(){
        if(getClickedPatient() == null){return;}
        showPatientInformationStage(true);
    }

    /**
     * Getting the clicked patient in the table view, displays a warning if no patients is clicked
     * @return Clicked patient
     */
    private Patient getClickedPatient(){
        if (tableView.getSelectionModel().getSelectedItem()== null){
            Alert noPatientClickedWarning = new Alert(Alert.AlertType.WARNING,"No patient clicked");
            noPatientClickedWarning.setTitle("Warning!");
            noPatientClickedWarning.show();
            return null;
        }
        return (Patient) tableView.getSelectionModel().getSelectedItem();
    }

    /**
     * Displays the edit or add patient page
     * Stage is saved in extraStage
     * @param editBoolean Edit or add
     */
    private void showPatientInformationStage(boolean editBoolean) {
        if(!(extraStage ==null)&&extraStage.isShowing()){
            Alert noPatientClickedWarning = new Alert(Alert.AlertType.WARNING,"You already have a new page open!");
            noPatientClickedWarning.setTitle("Warning!");
            noPatientClickedWarning.show();
        }
        else{
            PatientInformation patientInformation = (PatientInformation) FileHandler.loadView("PatientInformation");
            if(editBoolean){patientInformation.setPatient(getClickedPatient());}
            extraStage = new Stage();
            extraStage.setScene(new Scene((Parent) patientInformation.getParent()));
            extraStage.setTitle("Add Patient");
            extraStage.setResizable(false);
            extraStage.show();

        }
    }

    /**
     * Public static method to close any extra stages that has been created
     * Example add patient stage
     */
    public static void closeExtraStage(){
        extraStage.close();
        updateObservableList();
    }

    /**
     * Save file method, only able to save csv files
     */
    @FXML
    private void saveFile() {
        fileChooser.setTitle("Save File");
        File savedFile = fileChooser.showSaveDialog(parent.getScene().getWindow());
        if(savedFile == null){return;}
        if (savedFile.getName().endsWith(".csv")){
            FileHandler.saveCSVFile(savedFile);
        }
        else {
            Alert failedSave = new Alert(Alert.AlertType.WARNING, "Please save as csv File");
            failedSave.setTitle("Warning!");
            failedSave.show();
        }
    }

    /**
     * Load file method, only able to load csv
     */
    @FXML
    private void loadFile() {
        fileChooser.setTitle("Load File");
        File loadedFile = fileChooser.showOpenDialog(parent.getScene().getWindow());
        if (loadedFile == null) {
            return;
        }
        if (loadedFile.getName().endsWith(".csv")) {
            if (!FileHandler.loadCSVFile(loadedFile)) {
                Alert noPatientClickedWarning = new Alert(Alert.AlertType.WARNING, "Some of the loaded data was not successfully implemented");
                noPatientClickedWarning.setTitle("Warning!");
                noPatientClickedWarning.show();
            }
            updateObservableList();
        } else {
            Alert notCorrectFile = new Alert(Alert.AlertType.WARNING, "The chosen file is not a csv file");
            notCorrectFile.setTitle("Warning!");
            notCorrectFile.show();
        }
    }

    /**
     * Currently does not do anything,
     * could be used to better implement
     * a check to the application
     *
     *TODO Use this to make it more responsive
     */
    public void statusCheck(){
        if (status){ statusLabel.setText("Status: WORKING");}
        else statusLabel.setText("Status: FAILURE");
    }

    /**
     * Shows information about the application
     */
    @FXML
    private void about(){
        Alert aboutInfo = new Alert(Alert.AlertType.INFORMATION);
        aboutInfo.setTitle("About Application");
        aboutInfo.setHeaderText("Patient Register \n" +
                "Version 1.0");
        aboutInfo.setContentText("Application created by \n" +
                "Erlend Matre \n" +
                "05.05.2021");
        aboutInfo.show();
    }


    /**
     * Opens a page to edit or set diagnosis of clicked patient
     */
    @FXML
    private void setDiagnosis(){
        if(getClickedPatient() == null){return;}
        SetDiagnosis setDiagnosis = (SetDiagnosis) FileHandler.loadView("SetDiagnosis");
        setDiagnosis.setPatient(getClickedPatient());
        extraStage = new Stage();
        extraStage.setScene(new Scene((Parent) setDiagnosis.getParent()));
        extraStage.setTitle("Add Patient");
        extraStage.setResizable(false);
        extraStage.show();
    }

    /**
     * Opens a page to read the diagnosis of a clicked patient
     */
    @FXML
    private void readDiagnosis(){
        if(getClickedPatient() == null){return;}
        Alert diagnosisAlert = new Alert(Alert.AlertType.INFORMATION);
        diagnosisAlert.setTitle("Patient Diagnosis");
        diagnosisAlert.setHeaderText(getClickedPatient().getFirstName() + " " + getClickedPatient().getLastName());
        if(getClickedPatient().getDiagnosis() == null){
            diagnosisAlert.setContentText("No diagnosis");
        }
        else if(getClickedPatient().getDiagnosis().trim().isEmpty()){
            diagnosisAlert.setContentText("No diagnosis");
        }
        else diagnosisAlert.setContentText(getClickedPatient().getDiagnosis());
        diagnosisAlert.show();
    }
    @FXML
    /**
     * Closes the app
     */
    private void exit(){
        Client.exitApp();
    }
    /**
     * Gets the parent of this controller
     * @return AnchorPane (parent)
     */
    @Override
    public Node getParent() {
        return parent;
    }
}
