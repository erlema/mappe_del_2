package HospitalApplication.Data;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PatientTest {

    Patient patient = new Patient("Karl", "Hatt", "12121283953", "Marit Vag");
    Patient patientDifferent = new Patient("Peder", "Flaske", "02129885913", "Marit Vag");
    Patient patientSameSSN = new Patient("Sandra", "Kopp", "12121283953", "Marit Vag");
    @Test
    void getFirstNameTest() {
        assertEquals(patient.getFirstName(),"Karl");
    }

    @Test
    void setFirstNameTest() {
        patient.setFirstName("Mann");
        assertEquals(patient.getFirstName(),"Mann");
    }

    @Test
    void getLastNameTest() {
        assertEquals(patient.getLastName(),"Hatt");
    }

    @Test
    void setLastName() {
        patient.setLastName("Kvinne");
        assertEquals(patient.getLastName(),"Kvinne");
    }

    @Test
    void setSocialSecurityNumber() {
        assertEquals(patient.getSocialSecurityNumber(),"12121283953");
    }

    @Test
    void getSocialSecurityNumber() {
        patient.setSocialSecurityNumber("12345678901");
        assertEquals(patient.getSocialSecurityNumber(),"12345678901");
    }

    @Test
    void getDiagnosis() {
        assertEquals(patient.getDiagnosis(),null);
    }

    @Test
    void setDiagnosis() {
        patient.setDiagnosis("Sjuk");
        assertEquals(patient.getDiagnosis(),"Sjuk");
    }

    @Test
    void getGeneralPractitionersName() {
        assertEquals(patient.getGeneralPractitionersName(),"Marit Vag");
    }

    @Test
    void setGeneralPractitionersName() {
        patient.setGeneralPractitionersName("Husbu");
        assertEquals(patient.getGeneralPractitionersName(), "Husbu");
    }

    @Test
    void testEqualsDifferent() {
        assertFalse(patient.equals(patientDifferent));
    }
    @Test
    void testEqualsSame() {
        assertTrue(patient.equals(patientSameSSN));
    }

}