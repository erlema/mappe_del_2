package HospitalApplication.Data;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PatientRegisterTest {
    PatientRegister patientRegister = new PatientRegister();
    Patient patient = new Patient("Karl", "Hatt", "12121283953", "Marit Vag");
    Patient patientDifferent = new Patient("Peder", "Flaske", "02129885913", "Marit Vag");
    Patient patientSameSSN = new Patient("Sandra", "Kopp", "12121283953", "Marit Vag");

    @Test
    void addPatientSuccess() {
        assertTrue(patientRegister.addPatient(patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber(),
                patient.getGeneralPractitionersName()));
    }
    @Test
    void addPatientFailure() {
        patientRegister.addPatient(patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber(),
                patient.getGeneralPractitionersName());
        assertFalse(patientRegister.addPatient(patientSameSSN.getFirstName(),patientSameSSN.getLastName(), patientSameSSN.getSocialSecurityNumber(),
                patientSameSSN.getGeneralPractitionersName()));
    }

    @Test
    void removePatientSuccess() {
        patientRegister.addPatient(patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber(),
                patient.getGeneralPractitionersName());
        assertTrue(patientRegister.removePatient(patient));
    }
    @Test
    void removePatientFailure() {
        patientRegister.addPatient(patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber(),
                patient.getGeneralPractitionersName());
        assertFalse(patientRegister.removePatient(patientDifferent));
    }

    @Test
    void getPatientRegisterEmpty() {
        assertTrue(patientRegister.getPatientRegister().isEmpty());
    }
    @Test
    void getPatientRegisterWithContent() {
        patientRegister.addPatient(patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber(),
                patient.getGeneralPractitionersName());
        assertFalse(patientRegister.getPatientRegister().isEmpty());
    }
}
