package HospitalApplication;

import HospitalApplication.Data.Patient;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ClientTest {
    Patient patient = new Patient("Karl", "Hatt", "12121283953", "Marit Vag");
    Patient patientDifferent = new Patient("Peder", "Flaske", "02129885913", "Marit Vag");
    Patient patientSameSSN = new Patient("Sandra", "Kopp", "12121283953", "Marit Vag");

    @Nested
    public class AddPatient {

        @Test
        void addPatientCorrect() {
           assertTrue(Client.addPatient("Karl", "Hatt", "12312312321", "Marit Vag"));
        }
        @Test
        void addPatientWithSameSSN(){
            Client.addPatient(patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber(),
                    patient.getGeneralPractitionersName());
            assertFalse(Client.addPatient(patientSameSSN.getFirstName(),patientSameSSN.getLastName(), patientSameSSN.getSocialSecurityNumber(),
                    patientSameSSN.getGeneralPractitionersName()));
        }
        @Test
        void addPatientWithToLongFirstName(){
            assertFalse(Client.addPatient("ABCABCABCABCABCABCABCABCA",patient.getLastName(),patient.getSocialSecurityNumber(),
                    patient.getGeneralPractitionersName()));
        }
        @Test
        void addPatientWithToLongLastName(){
            assertFalse(Client.addPatient(patient.getFirstName(), "ABCABCABCABCABCABCABCABCA",patient.getSocialSecurityNumber(),
                    patient.getGeneralPractitionersName()));
        }
        @Test
        void addPatientWithToLongGenPractName(){
            assertFalse(Client.addPatient(patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber(),
                    "ABCABCABCABCABCABCABCABCAABCABCABCABCABCABCABCABCA"));
        }
        @Test
        void addPatientWithToLongSSN(){
            assertFalse(Client.addPatient(patient.getFirstName(),patient.getLastName(),"123456789012",
                    patient.getGeneralPractitionersName()));
        }
        @Test
        void addPatientWithToShortSSN(){
            assertFalse(Client.addPatient(patient.getFirstName(),patient.getLastName(),"1234567890",
                    patient.getGeneralPractitionersName()));
        }
        @Test
        void addPatientWithLetterInSSN(){
            assertFalse(Client.addPatient(patient.getFirstName(),patient.getLastName(),"1234567890A",
                    patient.getGeneralPractitionersName()));
        }
        @Test
        void addPatientWithNoFirstName(){
            assertFalse(Client.addPatient("",patient.getLastName(),patient.getSocialSecurityNumber(),
                    patient.getGeneralPractitionersName()));
        }
        @Test
        void addPatientWithNoLastName(){
            assertFalse(Client.addPatient(patient.getFirstName(), "",patient.getSocialSecurityNumber(),
                    patient.getGeneralPractitionersName()));
        }
        @Test
        void addPatientWithNoGenPractName(){
            assertFalse(Client.addPatient(patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber(),
                    ""));
        }

    }
    @Nested
    public class RemovePatient{
        @Test
        void removePatientCorrect(){
            Client.addPatient(patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber(),
                    patient.getGeneralPractitionersName());
            assertTrue(Client.removePatient(patient));
        }
        @Test
        void removePatientWrong(){
            Client.addPatient(patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber(),
                    patient.getGeneralPractitionersName());
            assertFalse(Client.removePatient(patientDifferent));
        }
    }

    @Nested
    public class EditPatient{
        @Test
        void editPatientCorrect() {
            Client.addPatient(patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber(),
                    patient.getGeneralPractitionersName());
            assertTrue(Client.editPatient(patient,patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber(),
                    patient.getGeneralPractitionersName()));
        }
        @Test
        void editPatientWithToLongFirstName(){
            Client.addPatient(patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber(),
                    patient.getGeneralPractitionersName());
            assertFalse(Client.editPatient(patient,"ABCABCABCABCABCABCABCABCA",patient.getLastName(),patient.getSocialSecurityNumber(),
                    patient.getGeneralPractitionersName()));
        }
        @Test
        void editPatientWithToLongLastName(){
            Client.addPatient(patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber(),
                    patient.getGeneralPractitionersName());
            assertFalse(Client.editPatient(patient,patient.getFirstName(), "ABCABCABCABCABCABCABCABCA",patient.getSocialSecurityNumber(),
                    patient.getGeneralPractitionersName()));
        }
        @Test
        void editPatientWithToLongGenPractName(){
            Client.addPatient(patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber(),
                    patient.getGeneralPractitionersName());
            assertFalse(Client.editPatient(patient,patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber(),
                    "ABCABCABCABCABCABCABCABCAABCABCABCABCABCABCABCABCA"));
        }
        @Test
        void editPatientWithToLongSSN(){
            Client.addPatient(patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber(),
                    patient.getGeneralPractitionersName());
            assertFalse(Client.editPatient(patient,patient.getFirstName(),patient.getLastName(),"123456789012",
                    patient.getGeneralPractitionersName()));
        }
        @Test
        void editPatientWithToShortSSN(){
            Client.addPatient(patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber(),
                    patient.getGeneralPractitionersName());
            assertFalse(Client.editPatient(patient,patient.getFirstName(),patient.getLastName(),"1234567890",
                    patient.getGeneralPractitionersName()));
        }
        @Test
        void editPatientWithLetterInSSN(){
            Client.addPatient(patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber(),
                    patient.getGeneralPractitionersName());
            assertFalse(Client.editPatient(patient,patient.getFirstName(),patient.getLastName(),"1234567890A",
                    patient.getGeneralPractitionersName()));
        }

        @Test
        void editPatientWithNoFirstName(){
            Client.addPatient(patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber(),
                    patient.getGeneralPractitionersName());
            assertFalse(Client.editPatient(patient,"",patient.getLastName(),patient.getSocialSecurityNumber(),
                    patient.getGeneralPractitionersName()));
        }
        @Test
        void editPatientWithNoLastName(){
            Client.addPatient(patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber(),
                    patient.getGeneralPractitionersName());
            assertFalse(Client.editPatient(patient,patient.getFirstName(), "",patient.getSocialSecurityNumber(),
                    patient.getGeneralPractitionersName()));
        }
        @Test
        void editPatientWithNoGenPractName(){
            Client.addPatient(patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber(),
                    patient.getGeneralPractitionersName());
            assertFalse(Client.editPatient(patient,patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber(),
                    ""));
        }
    }
    @Nested
    public class SetDiagnosis{
        @Test
        void setDiagnosisSuccess() {
           Client.addPatient(patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber(),
                    patient.getGeneralPractitionersName());
            assertTrue(Client.setDiagnosis(patient,"Sjuk"));
        }
        @Test
        void setDiagnosisWrongPatient() {
            Client.addPatient(patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber(),
                    patient.getGeneralPractitionersName());
            assertFalse(Client.setDiagnosis(patientDifferent, "Sjuk"));

        }
        @Test
        void setDiagnosisEmptyString() {
            Client.addPatient(patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber(),
                    patient.getGeneralPractitionersName());
            assertFalse(Client.setDiagnosis(patient, ""));
        }
    }

}